

# Test_Full_Stack_Frontend 

## Configuración del examen practico, Backend Laravel - Frontend React

El proyecto backend requiere trabajar con un entorno de MySQL

### MySQL con un contenedor docker 

1. Descargar e instalar docker, se puede encontrar aquí https://docs.docker.com/engine/install/
2. Ejecutar este comando en la raíz del repositorio: `docker run --name mysql-examen -e MYSQL_ROOT_PASSWORD=root -e MYSQL_USER=examen -e MYSQL_PASSWORD=examen -e MYSQL_DATABASE=examen -v $(pwd)/mysqldatadir:/var/lib/mysql -v $(pwd)/db:/docker-entrypoint-initdb.d --platform linux/x86_64 -p 3306:3306 -d mysql:5.7`
3. Quedará configurada una base de datos que se llama *examen* y los datos de conexión son host `localhost`, usuario `examen`, contraseña `examen` y puerto `3306`, para la visualización de los datos se podrá configurar la herramienta de tu elección, te recomendamos Sequel Pro http://www.sequelpro.com/docs/

El proyecto backend requiere de Composer https://getcomposer.org/download/  
El proyecto frontend requiere trabajar con NodeJS version 16.13 https://nodejs.org/en/ y yarn https://classic.yarnpkg.com/lang/en/docs/install


## Instalacion del proyecto backend

1. Ir a la carpeta de su proyecto backend: `cd backend`
2. Instalar las dependencias del proyecto: `composer install`
3. Configurar la app para conectarse con el servidor de MySQL corriendo en docker
4. Crear un nuevo archivo .env: `cp .env.example .env`
5. Agregar sus propias credenciales de base de datos en el archivo .env en DB_DATABASE, DB_USERNAME, DB_PASSWORD
6. Generar la clave de la aplicación: `php artisan key:generate`
7. Habilitar link simbolico a la carpeta storage para poder ver las imagenes subidas: `php artisan storage:link`

Para puebas puede ejecutar el proyecto con `php artisan serve`

Si esta configurado correctamente se podrá acceder a las rutas:

http://127.0.0.1:8000/api/properties y obtener lo siguiente:
-------------
    [{"id":1,"created_at":null,"updated_at":"2022-01-24T09:32:34.000000Z","public_key":"0001","name":"Casa en renta Condesa","description":"Se renta bonita casa en condesa","price":"15000.00","property_type":"APARTAMENT","operation":"RENT","state":"CDMX","city":"Cuauht\u00e9moc","neighborhood":"Condesa","cp":"6140","street":"Tamaulipas","latitude":"19.4114791","longitude":"-99.1762838","num_bathrooms":1,"bedrooms":2,"m2_construction":105,"parking":1,"age":15,"departments":0,"floor":1,"user":1}]
-------------

http://127.0.0.1:8000/api/properties/1 y obtener lo siguiente:
-------------
    {"id":1,"created_at":null,"updated_at":"2022-01-24T09:32:34.000000Z","public_key":"0001","name":"Casa en renta Condesa","description":"Se renta bonita casa en condesa","price":"15000.00","property_type":"APARTAMENT","operation":"RENT","state":"CDMX","city":"Cuauht\u00e9moc","neighborhood":"Condesa","cp":"6140","street":"Tamaulipas","latitude":"19.4114791","longitude":"-99.1762838","num_bathrooms":1,"bedrooms":2,"m2_construction":105,"parking":1,"age":15,"departments":0,"floor":1,"user":1}
-------------

## Instalacion del proyecto frontend

1. Ir a la carpeta del proyecto frontend: `cd frontend`
2. Instalar las dependencias del proyecto: `yarn install`
3. Crear un nuevo archivo .env: `cp .env.example .env`
4. `REACT_APP_API_URL` debe contener la URL de su proyecto backend (eg. http://localhost:8000/api)
5. `REACT_APP_API_KEY_GOOGLE_MAPS` debe contener su llave de google maps
6. `REACT_APP_IMAGE_URL` debe contener la URL del poyecto para imagenes (eg. http://localhost:8000)
7. Ejecute `yarn start` para iniciar la aplicación en un entorno de desarrollo local.

Si esta configurado correctamente se podrá acceder a la primera pantalla para el listado de las propiedades

# Examen Práctico:

Este examen está pensado para que se le dedique de 4-5 horas, no esperemos que este solucionado al 100%. Por lo mismo, estaremos evaluando no solo el resultado, si no la forma de implementación.

La inmobiliaria **HABI** requiere de una aplicación web para la administración de sus inmuebles con las siguientes consideraciones:

- El sistema deberá permitir la creación, lectura, borrado y edición de inmuebles, se proporciona carpeta backend en Laravel con estructura de una api rest.

-  La creación de inmuebles debe considerar los siguientes criterios: 

    - El formulario  de  registro  para los inmuebles debe contar con las siguientes restricciones:

        - Nombre del inmueble: validar solo el uso de textos

        - Clave del inmueble: respetar el siguiente formato **PCOM-XXX/##**

        - Galeria de imagenes: agregar 5 fotos de imagenes del inmueble y almacenarlas en base de datos como parte de su galería

        - Descripción, tipo de transacción (venta / renta), tipo de inmueble (casa, departamento, terreno, etc) y el precio del inmueble

    - Datos de ubicación mínimos requeridos: Estado, Delegación o municipio, Colonia, calle y número

    - Datos de geoposicionamiento: latitud y longitud

    - Características: Se puede agregar N características al inmueble (Ejemplo: cisterna, área de lavado, seguridad privada, etc)

- El sistema deberá permitir la edición de todos los campos antes mencionados a excepción de la clave del inmueble

- Deberá existir una sección donde se muestre un mapa con el **API de google maps** donde se visualicen los pins de los inmuebles registrados

    - La posicion del pin para cada inmueble en el mapa corresponde a su latitud y longitud registrada

    - Al seleccionar   cualquier   pin   del   mapa; debe mostrar el nombre del inmueble y poderle dar clic para entrar a su detalle.

- Deberá existir una sección donde se listaran los inmuebles registrados como se muestra en la siguiente imagen

    - El listado se podra ver desde un inicio

    - Los botones de edición y borrado será para todos los usuarios
    
    - Cada inmueble del listado, debera mostrar sobre la imagen clave del inmueble, calle y numero

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examen.png)

- Para las card’s el diseño es el siguiente:

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examenCard.png)

- Al dar click en un inmueble del listado, se debera abrir la ficha tecnica del inmueble como se muestra en la siguiente imagen

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examen2.png)

- Para el detalle de las card’s, el diseño es el siguiente:

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examenDetalle.png)

Agregando los elementos faltantes, como:

-   la cabecera

-   las características del inmueble

-   la ubicación, mapa.


# Importante:
    - Para el Frontend (se deberá usar la biblioteca de REACT)
        - Ya se entrega proyecto en carpeta frontend del repositorio.
        - El proyecto entregado utiliza redux.
        - Se entrega ejemplo que obtiene del backend el listado de las propiedades. 
    - El diseño se debe apegar a las imagenes proporcionadas, se tendra que maquetar y utilizar estilos custom, de preferencia no usar bootstrap.
    - La aplicación debera ser responsiva
    - Utilice algún compilador p. ej. SASS, LESS, etc.
    - Se analizará maqueta y estilos.
----------

Requerimientos de entrega:
-------------

- Repositorio: 
    - Debe contar con una cuenta en **bitbucket** para poder bifurcar (**fork**) el repositorio
    - Realice el examen práctico  sobre la copia que realizó
    - Al finalizar la prueba, enviar el link de su repositorio al siguiente correo:  **daniel.nava@propiedades.com** con los permisos adecuados para poder clonarlo.

Especificaciones:
-------------
- Tiene la libertad de modificar la estructura de la **BD** de acuerdo a sus necesidades.
- El uso de los commit debe contar con una descripción detallada.
- Al finalizar la prueba, en la carpeta **BD** crear el archivo **dump.sql** de la estructura final de la base de datos que requiere, e informar que se cambio la BD, si no, se revisara con la BD original.

**config.txt** 

-  En el archivo **config.txt**  especificar  la configuración que su examen requiere para que pueda funcionar correctamente.  Si realizo el uso de algún **framework** o **biblioteca** adicional, favor de especificar el nombre y la versión  para su adecuado funcionamiento.