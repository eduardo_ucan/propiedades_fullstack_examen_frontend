import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Form from '../views/Form.vue'
import Show from '../views/Show.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/property/add',
    component: Form
  },
  {
    path: '/property/:id/edit',
    component: Form
  },
  {
    path: '/property/:id/show',
    component: Show
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
//TODO: Agregar guard a las rutas que requieran verificar login y permisos