import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import * as VueGoogleMaps from 'vue2-google-maps'

window.axios = require('axios')
window.apiUrl = process.env.VUE_APP_API_URL
window.imageUrl = process.env.VUE_APP_IMAGE_URL

Vue.config.productionTip = false
Vue.prototype.$title = '';

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.VUE_APP_API_KEY_GOOGLE_MAPS,
    libraries: 'places',
  }
});

new Vue({
  router,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')