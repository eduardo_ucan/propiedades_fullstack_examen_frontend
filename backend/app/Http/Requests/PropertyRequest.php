<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Null;
        if($this->isMethod('put'))
            $id = ",{$this->id}";

        return [
            'public_key' => 'required|string|min:11|max:11|unique:App\Models\Property,public_key' . ($id ?? ''),
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'property_type' => 'required|string',
            'operation' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'neighborhood' => 'required|string',
            'cp' => 'required|numeric',
            'street' => 'required|string',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'num_bathrooms' => 'required|numeric',
            'bedrooms' => 'required|numeric',
            'm2_construction' => 'required|numeric',
            'parking' => 'required|numeric',
            'age' => 'required|numeric',
            'departments' => 'required|numeric',
            'floor' => 'required|numeric',
            'files' => 'required',
            'files.*' => 'file|image',
        ];
    }
}
