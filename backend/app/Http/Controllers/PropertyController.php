<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyRequest;
use App\Models\Image;
use App\Models\Property;


use Illuminate\Support\Facades\Storage;

//TODO: Implementar el uso de resources para enviar unicamente la información necesaria.
//TODO: Implementar middlewares en los metodos para restringir el acceso a los mismos.
class PropertyController extends Controller
{
    // Obtiene todos los recurso de la BD
    public function index(){
        $propeties = Property::with(['images' => function ($q){
            $q->orderBy('order','asc');
        }])->get();

        return $propeties;
    }
    
    public function show(Property $property){
        return $property->load(['images','amenities']);
    }

    // Almacena una propieda en la BD
    public function store(PropertyRequest $request){
        try {
            // Crea un nuevo elemento
            $property = Property::create($request->except('files'));
            // Guarda cada una de las imagenes recibidas
            $this->storeFiles($request, $property);
        } catch (\Throwable $th) {
            //TODO: Take action
        }
        return response()->json('OK');
    }

    // Actualiza una propiedad en la BD
    public function update(PropertyRequest $request, Property $property){
        try {
            //Actualizamos la propiedad
            $property->update($request->except('files'));
            //Eliminamos todas las imagenes relacionadas
            foreach($property->images as $image){
                Storage::disk('public')->delete($image->path);
                $image->delete();
            } 
            //Guardamos las nuevas imagenes
            $this->storeFiles($request, $property);
        } catch (\Throwable $th) {
            //TODO: Take action
        }
        return response()->json('OK');
    }

    // Guarda las imaganes de una propiedad
    public function storeFiles($request, Property $property){
        foreach($request->file('files') as $key => $file){
            $path = "properties/{$property->id}/";
            $name = "image-{$key}.{$file->getClientOriginalExtension()}";
            Storage::disk('public')->putFileAs($path, $file, $name);
            Image::create([
                'path' => $path.$name,
                'order' => $key,
                'property' => $property->id
            ]);
        }
    }
}
